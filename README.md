## MonaCS: ROS-based Control System for Mona Robot

### Repository File System
This group consists of several repositories to control the Mona Robot using Robot Operating Systems (ROS). These repositories are executed in three different platforms. These repositories are
1. [Ardumona](https://gitlab.com/MonaCS/ardumona)
    1. Platform: Mona Robot with Arduino Pro Mini
    2. Usage:
        - To receive/transmit data via *Serial Communication*
        - To acquire and process signal from sensors
        - To generate low-level control input (PWM signal of left and right motors) from received linear and angular velocities
2. [Monaros](https://gitlab.com/MonaCS/monaros)
    1. Platform: Raspberry Pi Zero W
    2. Operating System: Raspbian Stretch
    3. Dependencies:
        - ROS Kinetic (Follow [this instruction](https://gist.github.com/Tiryoh/ce64ad0d751a9c298b87dc059d6cca37) to install)
        - Python 2.x
    4. Usage:
        - To receive velocity command from PC (e.g., for centralised control)
        - To receive position of agents from PC (e.g., for decentralised control)
        - To transmit linear and angular velocity inputs to Mona Robot with Ardumona
        - To place the distributed controller (only receive a position from Central PC)
3. [Monacom](https://gitlab.com/MonaCS/monacom)
    1. Platform: Laptop
    2. Operating System: Ubuntu 16.04
    3. Dependencies:
        - ROS Kinetic (Follow [this instruction](http://wiki.ros.org/kinetic/Installation/Ubuntu) to install)
        - Pyton 2.x
        - A camera based positioning system, e.g., [Quarclon](https://gitlab.com/quarcle/Quarclon) or Vicon with [Vicon Bridge](http://wiki.ros.org/vicon_bridge) and [Vicon Mocap](http://wiki.ros.org/vicon_mocap)
    4. Usage:
        - To run the camera positioning system, or to receive position data from Vicon system
        - To place the centralised controller
        - To publish the position information to a rostopic that is ready to subscribe