### Steps in PC
1. install ros kinetic
2. setup ros workspace
3. install vicon_bridge
4. update IP .bashrc to change IP address
5. connect to local router (TP-LINK)
7. run vicon_bridge node

### Steps in raspberry
1. install ros kinetic
2. setup ros workspace
3. setup wpa_supplicant
4. connect to local router (TP-LINK)
5. update IP in .bashrc 192.168.1.101
6. subscribe from rostopics

### Steps in PC again
1. scan IP address to find IP of raspberry
